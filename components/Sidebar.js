"use client";

import Link from "next/link"
import { useEffect, useState } from "react";
import { useRouter } from 'next/navigation'
import { BiMap,BiLaptop, BiFontSize } from "react-icons/bi";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";

const Sidebar = (props) => {

    const router = useRouter()
 
  const { data: session } = useSession()

  const [providers, setProviders] = useState(null);

  useEffect(() => {
    (async () => {
      const res = await getProviders();
      setProviders(res);
    })();
  }, []);

  function handleSignOut(){
    signOut()
  }

  return (
<>
    {/* {session?.user ? ( */}

    <aside id="layout-menu" className="layout-menu menu-vertical menu bg-menu-theme">
        <div className="app-brand demo">
            <a href="SuperAdmin-Dashboard.php" className="app-brand-link">
                <span className="app-brand-logo demo">
                    <img src="/assets/img/pixel-logo.png" alt="" height="50px"/>
                </span>
            </a>

            <a className="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                <i className="bx bx-chevron-left bx-sm align-middle"></i>
            </a>
        </div>
        
        <div className="menu-inner-shadow"></div>

        <ul className="menu-inner py-1">

          
            <li className="menu-header small text-capitalize fw-bold"><span className="menu-header-text clr-theme">Working space </span></li>

            {(props.menu=='dashboard') ?
            <li className="menu-item active">
                <Link href="/adminDashboard" className="menu-link">
                <i className="menu-icon tf-icons bx bx-user-check"></i>
                    <div>Dashboard</div>
                </Link>
            </li>
            :
            <li className="menu-item">
            <Link href="/adminDashboard" className="menu-link">
            <i className="menu-icon tf-icons bx bx-user-check"></i>
                <div>Dashboard</div>
            </Link>
        </li>
    }
            {(props.menu=='school') ?
            <li className="menu-item active">
                <Link href="/school" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-circle"></i>
                    <div>Schools</div>
                </Link>
            </li>
            :
            <li className="menu-item">
                <Link href="/school" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-user-circle"></i>
                    <div>Schools</div>
                </Link>
            </li>
}
        {(props.menu=='grades') ?
            <li className="menu-item active">
                <Link href="/grades" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                    <div>Grades</div>
                </Link>
            </li>
            :
            <li className="menu-item">
                <Link href="/grades" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-chalkboard"></i>
                    <div>Grades</div>
                </Link>
            </li>
        }
            <li className="menu-item">
                <a href="teachers.php" className="menu-link">
                    <i className="menu-icon tf-icons bx bxs-user-account"></i>
                    <div>Teachers</div>
                </a>
            </li>
            <li className="menu-item">
                <a className="menu-link">
                    <i className="menu-icon tf-icons bx bx bx-buildings"></i>
                    <div>Students</div>
                </a>
            </li>
            {(props.menu=='district') ?
            <li className="menu-item active">
                <Link href="/district" className="menu-link">
                    <BiMap className="menu-icon" style={{fontSize:'large'}}/>
                    <div>School Districts</div>
                </Link>
            </li>
            :
            <li className="menu-item">
                <Link href="/district" className="menu-link">
                <BiMap className="menu-icon" style={{fontSize:'large'}}/>
                    <div>School Districts</div>
                </Link>
            </li>
            }
            <li className="menu-item">
                <a href="surveys.php" className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Surveys</div>
                </a>
            </li>
            {(props.menu=='district') ?
            <li className="menu-item active">
                <Link href="/lessons" className="menu-link">
                    <BiLaptop className="menu-icon" style={{fontSize:'large'}}/>
                    <div>Lessons</div>
                </Link>
            </li>
            :
            <li className="menu-item">
                <Link href="/lessons" className="menu-link">
                    <BiLaptop className="menu-icon tf-icons" style={{fontSize:'large'}}/>
                    <div>Lessons</div>
                </Link>
            </li>
        }
            <li className="menu-item">
                <a className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Resources</div>
                </a>
            </li>
            <li className="menu-item">
                <a className="menu-link">
                    <i className="menu-icon tf-icons bx bx-detail"></i>
                    <div>Reports</div>
                </a>
            </li>

            
            <li className="menu-header small text-capitalize fw-bold"><span className="menu-header-text clr-primary">Personal </span></li>
            <li className="menu-item">
                <a className="menu-link">
                    <i className="bx bx-cog me-2"></i>
                    <span className="align-middle me-2">Personal Info</span>
                </a>
            </li>
            <li className="menu-item">
                <a className="menu-link">
                    <i className="bx bx-cog me-2"></i>
                    <span className="align-middle me-2">Notifications</span>
                    <span className="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20 ms-auto">9</span>
                </a>
            </li>
        </ul>
    </aside>
    {/* ):
    <>
    {router.push('/login')}
    </>
    } */}
</>
  )
}

export default Sidebar